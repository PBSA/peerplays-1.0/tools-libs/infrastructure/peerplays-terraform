resource "aws_key_pair" "demo_key" {
  key_name   = "MyKeyPair"
  public_key = file(var.public_key)
}

/*
resource "aws_vpc" "my-vpc" {
  cidr_block           = "10.0.0.0/16" # Defines overall VPC address space
  enable_dns_hostnames = true          # Enable DNS hostnames for this VPC
  enable_dns_support   = true          # Enable DNS resolving support for this VPC
  instance_tenancy     = "default"
  enable_classiclink   = "false"
  tags {
    Name = "VPC-my-vpc" # Tag VPC with name
  }
}
*/

resource "aws_instance" "witness_node" {
  count = var.instance_count

  #ami = "${lookup(var.amis,var.region)}"
  ami           = var.ami
  instance_type = var.instance
  key_name      = aws_key_pair.demo_key.key_name

  vpc_security_group_ids = [
    aws_security_group.witness_api.id,
    aws_security_group.ssh.id,
    aws_security_group.egress_tls.id
  ]


  ebs_block_device {
    device_name           = "/dev/xvda"
    volume_size           = 20
    volume_type           = "gp2"
    encrypted             = false
    delete_on_termination = true
  }

  connection {
    private_key = file(var.private_key)
    user        = var.ansible_user
    host = self.public_ip
  }

  #user_data = "${file("../templates/install_jenkins.sh")}"

  # Ansible requires Python to be installed on the remote machine as well as the local machine.
  provisioner "remote-exec" {
    inline = [
      "sleep 10",
      "sudo apt-get update",
      "sleep 10", 
      "sudo apt-get install python -y"
    ]
  }

  # Copies the blockchain configuration files to the instance
  provisioner "file" {
    source      = "../../blockchain"
    destination = "/home/ubuntu"  
  }

  # Copies the witess service file to the instance
  provisioner "file" {
    source      = var.witness_service
    destination = "/tmp/witness.service"  
  }

  # This is where we configure the instance with ansible-playbook 
  provisioner "local-exec" {
    command = <<EOT
    sleep 30;
	  >witness_node.ini;
	  echo "[witness_node]" | tee -a witness_node.ini;
	  echo ${aws_instance.witness_node[count.index].public_ip} ansible_user=${var.ansible_user} ansible_ssh_private_key_file=${var.private_key} | tee -a witness_node.ini;
    export ANSIBLE_HOST_KEY_CHECKING=False;
	  ansible-playbook -u ${var.ansible_user} --private-key ${var.private_key} -i witness_node.ini ../playbooks/install_witness_node.yaml
    EOT
  }
}

resource "aws_security_group" "egress_tls" {
  name        = "default-egress-tls"
  description = "Default security group that allows inbound and outbound traffic from all instances"
  #vpc_id      = "${aws_vpc.my-vpc.id}"

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "witness_api" {
  name        = "default-witness-api"
  description = "Security group for witness_api that allows access to the RPC node (on port 8090)"
  #vpc_id      = "${aws_vpc.my-vpc.id}"

  ingress {
    from_port   = 8090
    to_port     = 8090
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "ssh" {
  name        = "default-ssh"
  description = "Security group for nat instances that allows SSH traffic from internet"
  #vpc_id      = "${aws_vpc.my-vpc.id}"

  ingress {
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
}